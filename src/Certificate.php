<?php namespace App\Certificates;

use App\Certificates\CertificateInterface;
use App\Certificates\CertificateException;
use App\Abante\Modules\Db;

abstract class Certificate implements CertificateInterface{
	protected $id;
	protected $src;
	protected $pwd;
	const typeSysnames = [
		1 => 'webinars',
		2 => 'contests_pupils',
		3 => 'contests_teachers'
	];

	public function __construct($id, $pwd){
		$this->id = $id;
		$this->pwd = $pwd;
	}

	abstract public function create($Name, $Email, $Job);

	abstract public function view();
	
	abstract public function fields();

	public function delete(){
		return @unlink($this->src);
	}

	public static function getLink($id){
		return route('certificates', ['certid' => $id, 'pwd' => self::getHash($id)]);
	}

	public static function getHash($id){
		$dt = getDate();
		return md5($id . 'somesaltM' . $dt['mday'] . $dt['mon'] . $dt['year']);
	}

	public static function getTypeSysname($typeId){
		return self::typeSysnames[$typeId];
	}

 	public function saveUser($attributes){
 		$id = $this->id;
 		$Name = mysql_real_escape_string($attributes['Name']);
 		$Email = mysql_real_escape_string($attributes['Email']);
		db::query("INSERT IGNORE INTO CertificateUsers SET CertificateID={$id}, Name='{$Name}', Email='{$Email}'");
 	}
 	
 	public function validateInput(){
		$fields = $this->fields();
		foreach ($fields as $field) {
			$value = input()->{$field};
			if(empty($value)){
				throw new CertificateException("Не заполнены все необходимые поля.");
			}
		}
	}
}