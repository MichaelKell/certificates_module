<?php namespace App\Certificates;

use App\Certificates\Certificate;
use App\Certificates\CertificateWebinar;
use App\Certificates\CertificatePupils;
use App\Certificates\CertificateTeachers;
use App\Certificates\CertificateException;
use App\Abante\Modules\Db;

class CertificateFactory{
	public static function create($certificateId, $pwd){
		if(empty($certificateId) || empty($pwd) || $pwd != Certificate::getHash($id)){
			throw new CertificateException('Ваша ссылка устарела. Пожалуйста, обратитесь за новой к администратору.');
		}
		$typeId = db::field("SELECT Type FROM Certificates WHERE __id=".intval($id));
		switch (Certificate::getTypeSysname($typeId)) {
			case 'webinars': $instance = new CertificateWebinar($certificateId, $pwd);
			case 'contests_pupils': $instance = new CertificatePupils($certificateId, $pwd);
			case 'contests_teachers': $instance = new CertificateTeachers($certificateId, $pwd);
			default: throw new \UnexpectedValueException("Unexpected certificate type");
		}
		return $instance;
	}
}