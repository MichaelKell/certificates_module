<?php

function drawText($im, $text, $fontSise, $padTop, $padLeft, $fontBold = false, $textColor = 0x000000){
	if($fontBold){
		$font = "assets/fonts/PTF75F.ttf";
	}else{
		$font = "assets/fonts/PTF55F.ttf";
	}
	imagettftext($im, $fontSise, 0, $padLeft, $padTop, $textColor, $fontName, convert_text($text));
}

function drawTextCenter($im, $text, $fontSise, $padTop, $fontBold = false, $textColor = 0x000000){
	$textbox = imagettfbbox($fontSise, 0, $fontBold, convert_text($text));
	$padLeft = imagesx($im) / 2 - ($textbox[2] - $textbox[0]) / 2;
	drawText($im, $text, $fontSise, $padTop, $padLeft, $fontBold, $textColor);
}

function splitString($string, $limit, $encoding = 'utf-8'){
	if(mb_strlen($string, $encoding) <= $limit){
		return [$string, ''];
	}
	$break = mb_strrpos(mb_substr($string, 0, $limit, $encoding), ' ', 0, $encoding);
	$first_line = mb_substr($string, 0, $break, $encoding);
	$second_line = mb_substr($string, $break, mb_strlen($string, $encoding), $encoding);
	return [$first_line, $second_line];
}

function replaceQuotes($string){
	$string = preg_replace('/"(\S|\d)/', '�$1', $string);
	$string = preg_replace('/(\S|\d)"/', '$1�', $string);
	return $string;
}

function convert_text($text){
	return	iconv('cp1251', 'utf-8', $text);
}