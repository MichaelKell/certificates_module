<?php namespace App\Certificates;

interface CertificateInterface{
	public function create($Name, $Email, $Job);
}