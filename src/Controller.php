<?php namespace App\Certificates;

use App\Certificates\CertificateFactory;
use App\Certificates\Certificate;
use App\Certificates\CertificateException;
use App\Abante\Modules\View;
use App\Abante\Modules\Mailer;
use App\Abante\Modules\Db;

class Controller{
	public static function actionCertificateSend($id, $pwd){
		try{
			$certificate = CertificateFactory::create($id, $pwd);
			$certificate->validateInput();
		}catch(CerificateException $e){
			return View::render('certificates.error', ['Header' => '��������� �����������. ��������� ������', 'Body' => $e->getMessage()]);
		}
		$Email = input()->Email;
		$Name = input()->Name;
		$src = $certificate->create();
		$mailAttachments = [
			[
				'path' => $src,
				'name' => pathinfo($src, PATHINFO_BASENAME),
				'disposition' => "attachment"
			]
		];
		$mailSubject = '��������� �����������';
		$mailBody = View::render('certificates.email_user');
		Mailer::send($Email, $mailSubject, $mailBody, $mailAttachments);
		$certificate->saveUser(['Name' => $Name, 'Email' => $Email]);
		$certificate->delete();
		$CertificateName = db::field("SELECT Name FROM Certificates WHERE __id = $id");
		return View::render('certificates.sent', ['Header' => '��������� ����������� '.$CertificateName, 'Body' => '���������� ��������� ��� �� �����', 'Email' => $Email]);
	}

	public static function actionCertificateForm($id, $pwd){
		try{
			$certificate = CertificateFactory::create($id, $pwd);
		}catch(CerificateException $e){
			return View::render('certificates.error', ['Header' => '��������� �����������. ��������� ������', 'Body' => $e->getMessage()]);
		}
		return $certificate->view();
	}
}