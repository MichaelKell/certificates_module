<?php namespace App\Certificates;

use App\Certificates\Certificate;
use App\Abante\Modules\View;
use App\Abante\Modules\Db;

class CertificatePupils extends Certificate{
	public function create($Name, $Email){
		$data = db::single('select Header, ColorText, Background, Treatment FROM Certificates WHERE __id='.$this->id);
		$name = md5($this->id . $Name . $Email.rand(1,100)) . '.jpg';
		$color = '0x'.$data['ColorText'];
		$im = imageCreateFromJpeg($data['Background']);
		drawTextCenter($im, $Name, 48, 1360, true, $color);
		$header = $data['Treatment'].' '.$data['Header'];
		$header = replaceQuotes($header);
		$header_arr = explode("\n", $header);
		if(count($header_arr) > 1){
			drawTextCenter($im, $header_arr[0], 30, 1480, false, $color);
			drawTextCenter($im, $header_arr[1], 30, 1540, false, $color);
		}else{
			$header_text = splitString($header, 60, 'cp1251');
			drawTextCenter($im, $header_text[0], 30, 1480, false, $color);
			drawTextCenter($im, $header_text[1], 30, 1540, false, $color);
		}
		$src = 'tmp/' . $name;
		imagejpeg($im, $src);
		imageDestroy($im);
		return $this->src = $src;
	}

	public function fields(){
		return ['Name', 'Email'];
	}

	public function view(){
		$ent['id'] = $this->id;
		$ent['pwd'] = $this->pwd;
		return View::render("certificates.pupils", $ent);
	}
}