<?php namespace App\Certificates;

use App\Certificates\Certificate;
use App\Abante\Modules\View;
use App\Abante\Modules\Db;

class CertificateWebinar extends Certificate{
	public function create($Name, $Email, $Job){
		$data = db::single('select Header, Background, DateTime, Hours FROM Certificates WHERE __id='.$this->id);
		$name = md5($this->id . $Name . $Job . $Email) . '.jpg';
		$im = imageCreateFromJpeg($data['Background']);
		drawTextCenter($im, "��������� ��������", 24, 500);
		$header = explode("\n", $data['Header']);
		if (!empty($header[1])) {
			drawTextCenter($im, '�' . trim($header[0]), 32, 550);
			drawTextCenter($im, trim($header[1]) . '�', 32, 600);
		} else {
			drawTextCenter($im, '�' . trim($header[0]) . '�', 32, 550);
		}
		drawTextCenter($im, $Name . ',', 40, 690);
		drawTextCenter($im, $Job, 20, 730);
		drawText($im, "����:", 16, 850, 576);
		$date = date('d.m.Y', strtotime($data['DateTime']));
		drawText($im, $date, 20, 850, 640);
		drawText($im, "���������� �����:", 16, 850, 965);
		drawText($im, $data['Hours'], 20, 850, 1159);
		$src = 'tmp/' . $name;
		imagejpeg($im, $src);
		imageDestroy($im);
		return $this->src = $src;
	}

	public function fields(){
		return ['Name', 'Email', 'Job'];
	}

	public function view(){
		$ent['id'] = $this->id;
		$ent['pwd'] = $this->pwd;
		return View::render("certificates.webinar", $ent);
	}
}